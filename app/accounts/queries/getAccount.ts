import { NotFoundError, SessionContext } from "blitz"
import db, { FindOneAccountArgs } from "db"
import getAccounts from "./getAccounts"

type GetAccountInput = {
  where: FindOneAccountArgs["where"]
  // Only available if a model relationship exists
  // include?: FindOneAccountArgs['include']
}

export default async function getAccount(
  { where /* include */ }: GetAccountInput,
  ctx: { session?: SessionContext } = {}
) {
  ctx.session!.authorize()

  const data = await getAccounts({ where, take: 1 }, ctx)
  const account = data.accounts[0]

  if (!account) throw new NotFoundError()

  return account
}
