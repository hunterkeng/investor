import React, { Suspense, useState } from "react"
import Layout from "app/layouts/Layout"
import { Head, Link, usePaginatedQuery, useRouter, BlitzPage } from "blitz"
import getAccounts from "app/accounts/queries/getAccounts"

const ITEMS_PER_PAGE = 100

export const AccountsList = () => {
  const router = useRouter()
  const page = Number(router.query.page) || 0
  const [{ accounts, hasMore }] = usePaginatedQuery(getAccounts, {
    orderBy: { id: "asc" },
    skip: ITEMS_PER_PAGE * page,
    take: ITEMS_PER_PAGE,
  })

  const goToPreviousPage = () => router.push({ query: { page: page - 1 } })
  const goToNextPage = () => router.push({ query: { page: page + 1 } })

  return (
    <div>
      <ul>
        {accounts.map((account) => (
          <li key={account.id}>
            <Link href="/accounts/[accountId]" as={`/accounts/${account.id}`}>
              <a className="w-1/2">{account.name}</a>
            </Link>
            {account.verified ? (
              <input
                className="ml-10"
                type="checkbox"
                checked={account.verified}
                onChange={() => {}}
              ></input>
            ) : null}
          </li>
        ))}
      </ul>

      <button disabled={page === 0} onClick={goToPreviousPage} className="btn-clear mt-4">
        Previous
      </button>
      <button disabled={!hasMore} onClick={goToNextPage} className="btn-clear ml-4 mt-4">
        Next
      </button>
    </div>
  )
}

const AccountsPage: BlitzPage = () => {
  return (
    <div>
      <Head>
        <title>Accounts</title>
      </Head>

      <main>
        <h1 className="text-4xl">Accounts</h1>

        <p className="my-6">
          <Link href="/accounts/new">
            <a className="btn-purple">Create Account</a>
          </Link>
        </p>

        <Suspense fallback={<div>Loading...</div>}>
          <AccountsList />
        </Suspense>
      </main>
    </div>
  )
}

AccountsPage.getLayout = (page) => <Layout title={"Accounts"}>{page}</Layout>

export default AccountsPage
