import { SessionContext, NotFoundError } from "blitz"
import db, { ApplicationCreateArgs } from "db"

import getAccounts from "app/accounts/queries/getAccounts"

type CreateApplicationInput = {
  data: ApplicationCreateArgs["data"]
}

export default async function createApplication(
  { data }: CreateApplicationInput,
  ctx: { session?: SessionContext } = {}
) {
  ctx.session!.authorize()

  const { accounts } = await getAccounts({}, ctx)

  const account = accounts?.filter((account) => account.id === Number(data.primaryAccountId))

  if (!account.length) throw new NotFoundError()

  const application = await db.application.create({
    data: {
      primaryAccountId: account[0].id,
      secondaryAccountId: accounts[0].id,
    },
  })

  return application
}
