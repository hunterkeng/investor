import { Account } from "@prisma/client"
import React from "react"

type ApplicationFormProps = {
  initialValues: Account[]
  onSubmit: React.FormEventHandler<HTMLSelectElement>
}

const ApplicationForm = ({ initialValues, onSubmit }: ApplicationFormProps) => {
  const accounts =
    initialValues &&
    initialValues
      .filter((primaryAccount) => primaryAccount.verified)
      .map((primaryAccount) => {
        return (
          <option key={primaryAccount.id} value={primaryAccount.id}>
            {primaryAccount.name}
          </option>
        )
      })

  const onSelect = (event) => {
    return () => {
      onSubmit(event)
    }
  }

  return (
    <form
      className="my-6"
      onSubmit={(event) => {
        event.preventDefault()
        onSubmit(event)
      }}
    >
      <select
        onBlur={(event) => onSelect(event)}
        className="block appearance-none w-1/3 bg-gray-200 border border-gray-200 text-gray-700 py-3 px-4 pr-8 rounded leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
      >
        {accounts}
      </select>
      <br></br>
      <button className="btn-purple mt-6">Submit</button>
    </form>
  )
}

export default ApplicationForm
