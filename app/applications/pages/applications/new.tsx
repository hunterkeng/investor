import React from "react"
import Layout from "app/layouts/Layout"
import { Head, Link, useRouter, BlitzPage, useQuery } from "blitz"
import createApplication from "app/applications/mutations/createApplication"
import ApplicationForm from "app/applications/components/ApplicationForm"

import { Suspense } from "react"

import getAccounts from "app/accounts/queries/getAccounts"

const NewApplication = () => {
  const router = useRouter()
  const [{ accounts }] = useQuery(getAccounts, {})

  const unverifiedAccountList = accounts
    .filter((primaryAccount) => !primaryAccount.verified)
    .map((primaryAccount) => {
      return (
        <li key={primaryAccount.id} value={primaryAccount.id} className="text-lg mt-5 opacity-75">
          <a href={`/accounts/${primaryAccount.id}`} className="underline">
            {primaryAccount.name}
          </a>
        </li>
      )
    })

  return (
    <div>
      <h1 className="text-2xl">Select Verified Accounts to Create New Application </h1>
      <ApplicationForm
        initialValues={accounts}
        onSubmit={async (event) => {
          try {
            const application = await createApplication({
              data: { primaryAccountId: event.target[0].value },
            })
            alert("Success!" + JSON.stringify(application))
            router.push("/applications/[applicationId]", `/applications/${application!.id}`)
          } catch (error) {
            alert("Error creating application " + JSON.stringify(error, null, 2))
          }
        }}
      />

      <p className="text-2xl">Accounts must be verified first before apply, click to verify</p>
      <ul className="mt-5">{unverifiedAccountList}</ul>
    </div>
  )
}

const NewApplicationPage: BlitzPage = () => {
  return (
    <div>
      <Head>
        <title>New Application</title>
      </Head>

      <main>
        <Suspense fallback="Loading...">
          <NewApplication />
        </Suspense>
      </main>
    </div>
  )
}

NewApplicationPage.getLayout = (page) => <Layout title={"Create New Application"}>{page}</Layout>

export default NewApplicationPage
