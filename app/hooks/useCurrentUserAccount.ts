import { useSession, useQuery } from "blitz"
import getCurrentUserAccount from "app/users/queries/getCurrentUserAccount"

export const useCurrentUserAccount = () => {
  // We wouldn't have to useSession() here, but doing so improves perf on initial
  // load since we can skip the getCurrentUser() request.
  const session = useSession()
  const [account] = useQuery(getCurrentUserAccount, null, { enabled: !!session.userId })
  return session.userId ? account : null
}
