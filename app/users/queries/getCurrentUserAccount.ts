import db from "db"
import { SessionContext } from "blitz"

export default async function getCurrentUserAccount(
  _ = null,
  ctx: { session?: SessionContext } = {}
) {
  if (!ctx.session?.userId) return null

  const account = await db.user
    .findOne({
      where: { id: ctx.session!.userId },
      select: { id: true, name: true, email: true, role: true, account: true },
    })
    .account()

  return account
}
